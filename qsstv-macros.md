# QSSTV Macros
[Source] https://web.archive.org/web/20220831223935/http://users.telenet.be/on4qz/qsstv/manual/macros.html



## Macro Text
*You can use macros to expand text add runtime. When layouting templates remember that % macros will be replaced by the final text.*

*Example: v will expand to QSSTV.9.4.3. So leave enough room to place the final text.*

### ***Following macro's are supported:***

### Macro's where the value is defined in the configuration

1. %m - my callsign
2. %q - my QTH
3. %l - my locator
4. %n - my last name
5. %f - my first name

#### Macro's where the value is entered in the TX-window prior to transmission*

1. %c - call of the contacted station
2. %r - RSV (radio-strength-video best is 595) You can also input free text as is sometimes required for contest (e.g. 595#007 or Good Copy)

3. %o - name of the operator of the contacted station
4. %x - comment1
5. %y - comment2
6. %z - comment3

*Note: comment 1,2 and 3 can be multi-line.*

##### Macro's where the value is defined by the system

1. %t - time in hours:minutes format
2. %d - date in year/month/day format
3. %v - qsstv_version
4. %s - SNR only in DRM mode

*in version 9.3 only used in the WF Text see also Statusbar*

*in version 9.4 and higher used in the WF Text and in the template images*

